'use strict'
var express = require('express');
var LineasContratoController = require('../controllers/lineasContrato');
var api = express.Router();

api.get('/', LineasContratoController.getLineasContrato);
api.get('/:idLineaContrato', LineasContratoController.getLineaContrato);
api.patch('/:idLineaContrato', LineasContratoController.patchLineaContrato);
api.post('/', LineasContratoController.postLineaContrato);
api.delete('/:idLineaContrato', LineasContratoController.deleteLineaContrato);

module.exports = api;
