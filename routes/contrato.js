'use strict'
var express = require('express');
var ContratoController = require('../controllers/contrato');
var api = express.Router();

api.get('/', ContratoController.getContratos);
api.get('/:idContrato', ContratoController.getContrato);
api.patch('/', ContratoController.patchContrato);
api.post('/', ContratoController.postContrato);
api.delete('/:idContrato/:idProyecto', ContratoController.deleteContrato);

module.exports = api;