var express = require('express');
var router = express.Router();

var Permission = require('../models/permission');
var Usuario = require('../models/usuario');

/**
 * Permission GET
 */
router.get('/', function (req, res) {
    Permission.find({}, function (err, doc) {
        res.send(doc);
    });
});


/**
 * Permissions GET con id de user
 */
router.get('/:idUser', function (req, res) {

    var user;
    var permissions = [];

    Usuario.findById(req.params.idUser, function (err, usr) {

        if (err) {
            return res.status(404).json({
                title: 'Ha ocurrido un error',
                error: err
            });
        }
        if (!usr) {
            return res.status(404).json({
                title: 'Error',
                error: 'Usuario no encontrada'
            });
        }
        user = usr;

        var prom = user.permissions.map(function (elem) {
            return new Promise(function (resolve) {
                Permission.findById(elem, function (err, doc) {
                    resolve(permissions.push(doc));
                });
            });
        });

        var prom2 = Promise.all(prom);
        prom2.then(function (respuestaPermissions) {
            res.status(200).json({
                message: 'Success',
                obj: tanques
            });

            //obs: obj podria ser tarqnuilamente tanques (el arreglo)
        });
    });
});

module.exports = router;