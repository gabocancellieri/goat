'use strict'
var express = require('express');
var ContactoController = require('../controllers/contacto');
var api = express.Router();

api.get('/:idEmpresa', ContactoController.getContactos);
api.get('/:idContacto', ContactoController.getContacto);
api.patch('/', ContactoController.patchContacto);
api.post('/', ContactoController.postContacto);
api.delete('/:idEmpresa/:idContacto', ContactoController.deleteContacto);

module.exports = api;