'use strict'
var express = require('express');
var ProyectoController = require('../controllers/proyecto');
var api = express.Router();

api.get('/', ProyectoController.getProyectos);
api.get('/:idProyecto', ProyectoController.getProyecto);
api.patch('/', ProyectoController.patchProyecto);
api.post('/', ProyectoController.postProyecto);
api.delete('/:idProyecto', ProyectoController.deleteProyecto);

module.exports = api;