'use strict'
var express = require('express');
var PlantaController = require('../controllers/planta');
var api = express.Router();
var auth = require("../auth.js")();

api.get('/:idEmpresa', auth.authenticate(), PlantaController.getPlantas);
api.get('/:idPlanta', PlantaController.getPlanta);
api.patch('/', PlantaController.patchPlanta);
api.post('/', PlantaController.postPlanta);
api.delete('/:idEmpresa/:idPlanta', PlantaController.deletePlanta);

module.exports = api;