var express = require('express');
var router = express.Router();

var Proveedor = require('../models/proveedor');

//Cambio

/**
 * Proveedor Schema
 */
router.get('/', function (req, res) {
  Proveedor.find({}, function (err, doc) {
    res.send(doc);
  })

});

router.post('/', function (req, res) {
  var proveedor = new Proveedor({
    nombre: req.body.nombre,
    direccion: req.body.direccion,
    telefono: req.body.telefono,
    email: req.body.email,
    contacto: req.body.contacto,
    telefonoContacto: req.body.telefonoContacto,
    cuit: req.body.cuit
  });

  proveedor.save().then(function (loc) {
    res.send(proveedor);
    //res.send('Proveedor guardado exitosamente');
  }, function (err) {
    res.send(String(err));
  });
});

module.exports = router;