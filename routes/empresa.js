'use strict'
var express = require('express');
var EmpresasController = require('../controllers/empresa');
var api = express.Router();

api.get('/', EmpresasController.getEmpresas);
api.get('/:idEmpresa', EmpresasController.getEmpresa);
api.patch('/', EmpresasController.patchEmpresa);
api.post('/', EmpresasController.postEmpresa);
api.delete('/:idEmpresa', EmpresasController.deleteEmpresa);

module.exports = api;