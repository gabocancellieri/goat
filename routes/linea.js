'use strict'
var express = require('express');
var LineasController = require('../controllers/linea');
var api = express.Router();

api.get('/:idContrato', LineasController.getLineas);
api.get('/:idLinea', LineasController.getLinea);
api.patch('/', LineasController.patchLinea);
api.post('/', LineasController.postLinea);
api.delete('/:idLinea/:idContrato', LineasController.deleteLinea);

module.exports = api;