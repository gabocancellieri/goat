'use strict'
var express = require('express');
var CertificadoController = require('../controllers/certificado');
var api = express.Router();

api.get('/', CertificadoController.getCertificados);
api.get('/certificadosProyectos', CertificadoController.getCertificadosConProyectos);
api.get('/:idProyecto', CertificadoController.getCertificadosProyecto);
api.get('/:idCertificado', CertificadoController.getCertificado);
api.patch('/', CertificadoController.patchCertificado);
api.post('/', CertificadoController.postCertificado);
api.delete('/:idCertificado/:idProyecto', CertificadoController.deleteCertificado);

module.exports = api;