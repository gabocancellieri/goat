'use strict'
var express = require('express');
var TanqueController = require('../controllers/tanque');
var api = express.Router();

api.get('/:idPlanta', TanqueController.getTanques);
api.get('/:idTanque', TanqueController.getTanque);
api.patch('/', TanqueController.patchTanque);
api.post('/', TanqueController.postTanque);
api.delete('/:idPlanta/:idTanque', TanqueController.deleteTanque);

module.exports = api;