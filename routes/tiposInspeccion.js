'use strict'
var express = require('express');
var TiposInspeccionController = require('../controllers/tiposInspeccion');
var api = express.Router();

api.get('/', TiposInspeccionController.getTiposInspeccion);

module.exports = api;
