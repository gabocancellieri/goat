'use strict'
var express = require('express');
var FojaController = require('../controllers/foja');
var api = express.Router();

api.get('/:idProyecto', FojaController.getFojas);
api.get('/:idFoja', FojaController.getFoja);
api.patch('/', FojaController.patchFoja);
api.post('/', FojaController.postFoja);
api.delete('/:idFoja/:idProyecto', FojaController.deleteFoja);

module.exports = api;