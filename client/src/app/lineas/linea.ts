import { LineaContrato } from '../lineasContrato/lineaContrato';
import { Ajuste } from './ajuste';

export class Linea {
    _id: string;
    lineaContrato: LineaContrato;
	ajustes: [Ajuste];
}