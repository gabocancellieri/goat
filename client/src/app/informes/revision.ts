import { Usuario } from '../usuario/usuario';

export class Revision {
    _id: string;
    numero: string;
	fecha: Date;
	observaciones: string;
	usuario: Usuario;
}