import { Revision } from './revision';

export class Informe {
    _id: string;
    nombre: string;
	fecha: Date;
	descripcion: string;
	revisiones: [Revision];
}