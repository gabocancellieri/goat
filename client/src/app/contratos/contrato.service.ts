import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';

import 'rxjs/add/operator/toPromise';

import { Contrato } from './contrato';
import {UsuarioService} from '../usuario/user.service';
import { UrlService } from '../window.provider.service';

@Injectable()
export class ContratoService {

    private headers = new Headers({'Content-Type': 'application/json'});
    private contratosURL =  this.urlService.getRestApiUrl() + '/contrato';  // URL to web api

    constructor(
        private http: Http,
        private userService: UsuarioService,
        private urlService: UrlService) { }

    private jwt() {
        return this.userService.jwt();
    }

    getContratos(): Promise<Contrato[]> {
        return this.http.get(this.contratosURL, this.jwt())
        .toPromise()
        .then(response => response.json() as Contrato[])
        .catch(this.handleError);
    }

    getContratosEspecifico(numProy: string): Promise<Contrato[]> {

        return this.http.get(this.contratosURL + '/' + numProy, this.jwt())
        .toPromise()
        .then(response => response.json().obj as Contrato[])
        .catch(this.handleError);
    }

    getContrato(idProy: string, idContrato: string): Promise<Contrato> {
        return this.http.get(this.contratosURL + '/' + idProy + '/' + idContrato, this.jwt())
        .toPromise()
        .then(response => {
            return response.json().obj as Contrato;
        })
        .catch(this.handleError);
    }

    postContrato(num: string, montoTot: number, fech: Date, fechInicio: Date,
                 fechFin: Date, formAj: string, tipoContrato: string, numProy: string, lin: string[]): Promise<Contrato> {
        return this.http
        .post(this.contratosURL, JSON.stringify({numero: num, montoTotal: montoTot,
                fecha: fech, fechaInicio: fechInicio, fechaFin: fechFin, formulaAjuste: formAj,
                tipo: tipoContrato, proyecto: numProy, lineas: lin}), {headers: this.headers})
        .toPromise()
        .then(res => {
            return res.json().obj;
        })
        .catch(this.handleError);
        }

    updateContrato(id: string, nomb: string, num: string, fechInicio: Date, fechFin: Date,
                   mont: number, formu: string, tipoContrato: string): Promise<Contrato> {
        return this.http
        .patch(this.contratosURL + '/' + id, JSON.stringify(
        {_id: id, nombre: nomb, numero: num, fechaInicio: fechInicio, fechaFin: fechFin,
        montoTotal: mont, formulaAjuste: formu, tipo: tipoContrato}), {headers: this.headers})
        .toPromise()
        .then(res => {
            return res.json().obj;
        })
        .catch(this.handleError);
    }

    deleteContrato(id: string, idProyecto: string): Promise<any> {
        const opciones = this.jwt();
        opciones.headers.append('Content-Type', 'application/json');
        return this.http
        .delete(this.contratosURL + '/' + id + '/' + idProyecto,
            opciones)
        .toPromise()
        .then(res => {
            return res.json().obj;
        })
        .catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
        console.error('Ocurrio un error en servicio de Contratos: ', error);
        alert(error.json().error);
        return Promise.reject(error.message || error);
    }
}