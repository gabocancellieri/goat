import { Component, OnInit } from '@angular/core';
import { ContratoService } from './contrato.service';
import { Contrato } from './contrato';

@Component({
  selector: 'app-contratos',
  templateUrl: './contratos.component.html'
})
export class ContratosComponent implements OnInit {

  contratos: Contrato[];
  cols: any[];

  constructor(
    private contratoService: ContratoService
  ) { }

  ngOnInit() {
    this.getContratos();
    this.cols = [
      { field: 'nombre', header: 'Nombre' },
      { field: 'numero', header: 'Numero' },
      { field: 'fechaInicio', header: 'Fecha Inicio' },
      { field: 'fechaInicio', header: 'Fecha Inicio' },
  ];
  }

// ***********
// *** GET ***
// ***********
  getContratos(): void {
      this.contratoService
      .getContratos()
      .then(contratos => {
          this.contratos = contratos;
          console.log(this.contratos);
      });
  }
}
