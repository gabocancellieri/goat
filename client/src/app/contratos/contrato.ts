import { Adenda } from './adenda';
import { Proyecto } from '../proyectos/proyecto';
import { Empresa } from '../empresas/empresa';
import { Certificado } from '../certificados/certificado';
import { Foja } from '../fojas/foja';
import { TipoContrato } from './tipoContrato';

export class Contrato {
    _id: string;
    nombre: string;
	numero: string;
    fecha: Date;
	fechaInicio: Date;
    formAjuste: String;
    tipo: TipoContrato;
    proyecto: Proyecto;
    empresa: Empresa;
    certificados: [Certificado];
    fojas: [Foja];
	adendas: [Adenda];
}
