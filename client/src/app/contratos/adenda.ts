import { Linea } from '../lineas/linea';

export class Adenda {
    _id: string;
    fechaAdenda: Date;
    fechaFin: Date;
    montoTotal: number;
    lineas: [Linea]
}
