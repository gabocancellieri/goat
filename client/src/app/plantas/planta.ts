import { Contacto } from "../contactos/contacto";
import { Tanque } from "../tanques/tanque";

export class Planta {
    _id: string;
    nombre: string;
    ubicacion: string;
    responsable: string;
    contacto: string;
    tanques: [Tanque];
    encargado: Contacto;
}