export class TipoInspeccion {
    _id: string;
    nombre: string;
    descripcion: string;
    tanque: boolean;
}