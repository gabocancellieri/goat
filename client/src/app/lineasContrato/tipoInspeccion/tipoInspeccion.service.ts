import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import Swal from 'sweetalert2';
import 'rxjs/add/operator/toPromise';

import { UrlService } from '../../window.provider.service';
import { TipoInspeccion } from './tipoInspeccion';

@Injectable()
export class TipoInspeccionService {

    private headers = new Headers({ 'Content-Type': 'application/json' });
    private tipoInspeccionURL = this.urlService.getRestApiUrl() + '/tipoInspeccion';  // URL to web api

    constructor(
        private http: Http,
        private urlService: UrlService) { }


    getTiposInspeccion(): Promise<TipoInspeccion[]> {
        return this.http.get(this.tipoInspeccionURL)
            .toPromise()
            .then(response => response.json().obj as TipoInspeccion[])
            .catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
        console.error('Ocurrio un error en servicio de Tipos de Inspeccion: ', error);
        Swal.fire(
            'Error!',
            error.json().error,
            'error'
        );
        return Promise.reject(error.message || error);
    }
}
