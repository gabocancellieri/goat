import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import Swal from 'sweetalert2';

import { LineaContratoService } from './lineaContrato.service';
import { LineaContrato } from './lineaContrato';
import { TipoInspeccionService } from './tipoInspeccion/tipoInspeccion.service';
import { TipoInspeccion } from './tipoInspeccion/tipoInspeccion';
import { NgForm } from '@angular/forms';

@Component({
    selector: 'app-lineas-contrato',
    templateUrl: './lineasContrato.component.html'
})

export class LineasContratoComponent implements OnInit {

    @ViewChild('cerrarAgregar') cerrarAgregar: ElementRef;
    @ViewChild('cerrarEditar') cerrarEditar: ElementRef;

    model: any = {};
    cols: any[];
    lineasContrato: LineaContrato[];
    selectedLineaContrato: LineaContrato;

    tiposInspeccion: TipoInspeccion[];

    constructor(
        private lineasContratoService: LineaContratoService,
        private tipoInspeccionService: TipoInspeccionService

    ) { }

    ngOnInit() {
        this.getLineasContrato();
        this.getTiposInspeccion();
        this.cols = [
            { field: 'nombre', header: 'Nombre' },
            { field: 'tipoInspeccion', header: 'Tipo de Inspeccion' }
        ];
    }

// ***********
// *** GET ***
// ***********
    getLineasContrato(): void {
        this.lineasContratoService
            .getLineasContrato()
            .then(lineasContrato => {
                this.lineasContrato = lineasContrato;
            });
    }

    getTiposInspeccion(): void {
        this.tipoInspeccionService
            .getTiposInspeccion()
            .then(tiposInspeccion => {
                this.tiposInspeccion = tiposInspeccion;
            });
    }

// ***************
// *** AGREGAR ***
// ***************
    agregarLineaContrato(f: NgForm): void {
        let idTipoInspeccion = null;
        if (this.model.tipoInspeccion) {
            idTipoInspeccion = this.model.tipoInspeccion._id;
        }

        this.lineasContratoService
            .postLineaContrato(this.model.nombre, idTipoInspeccion)
            .then(lineaContratoGuardada => {
                // Cerrar Modal
                this.cerrarAgregar.nativeElement.click();

                // Agrego elemento al arreglo
                this.lineasContrato.push(lineaContratoGuardada);

                // Mensaje de Éxito
                Swal.fire({
                    title: 'Agregada!',
                    text: 'Se ha agregado la línea de contrato correctamente.',
                    type: 'success',
                    timer: 4000
                }).then(
                    () => {},
                    // handling the promise rejection
                    (dismiss) => {
                        if (dismiss === 'timer') {

                        }
                    }
                );

                // Reiniciar los campos del formulario
                this.model = {};
                f.resetForm();
            });
    }

// ******************
// *** ACTUALIZAR ***
// ******************
    actualizarLineaContrato(): void {
        this.lineasContratoService
            .patchLineaContrato(this.selectedLineaContrato._id, 
                                this.selectedLineaContrato.nombre, 
                                this.selectedLineaContrato.tipoInspeccion._id)
            .then(lineaContratoActualizada => {
                // Cerrar Modal
                this.cerrarEditar.nativeElement.click();

                // Mensaje de Éxito
                Swal.fire({
                    title: 'Actualizada!',
                    text: 'Se ha actualizado la línea de contrato correctamente.',
                    type: 'success',
                    timer: 4000
                }).then(
                    () => {},
                    // handling the promise rejection
                    (dismiss) => {
                        if (dismiss === 'timer') {

                        }
                    }
                );
            });
    }

// ****************
// *** ELIMINAR ***
// ****************
    eliminarLineaContrato() {
        Swal.fire({
            title: 'Seguro?',
            text: 'Los cambios no se podrán revertir!',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Eliminar!'
        }).then((result) => {
            if (result.value) {
                this.lineasContratoService.deleteLineaDeContrato(this.selectedLineaContrato._id)
                .then(lineaContratoEliminada => {
                    // Mensaje de Exito
                    Swal.fire(
                        'Eliminada!',
                        'Se ha eliminado la Linea de Contrato.',
                        'success'
                    );

                    // Eliminamos el elemento del arreglo
                    let i: number;
                    this.lineasContrato.forEach((elem, index) => {
                        if (elem._id === lineaContratoEliminada._id) {
                            i = index;
                        }
                    });
                    this.lineasContrato.splice(i, 1);
                });
            }
        });
    }
}
