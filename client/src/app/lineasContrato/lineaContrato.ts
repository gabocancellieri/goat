import { TipoInspeccion } from './tipoInspeccion/tipoInspeccion';

export class LineaContrato {
    _id: string;
    nombre: string;
    tipoInspeccion: TipoInspeccion;
}