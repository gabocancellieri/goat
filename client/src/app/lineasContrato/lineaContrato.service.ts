import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import Swal from 'sweetalert2';
import 'rxjs/add/operator/toPromise';

import { LineaContrato } from './lineaContrato';
import { UrlService } from '../window.provider.service';

@Injectable()
export class LineaContratoService {

    private headers = new Headers({ 'Content-Type': 'application/json' });
    private lineaContratoURL = this.urlService.getRestApiUrl() + '/lineaContrato';  // URL to web api

    constructor(
        private http: Http,
        private urlService: UrlService) { }

// ***********
// *** GET ***
// ***********
    getLineasContrato(): Promise<LineaContrato[]> {
        return this.http.get(this.lineaContratoURL)
            .toPromise()
            .then(response => response.json().obj as LineaContrato[])
            .catch(this.handleError);
    }

    getLineaContrato(idLineaCont: string): Promise<LineaContrato> {
        return this.http.get(this.lineaContratoURL + '/' + idLineaCont)
            .toPromise()
            .then(response => response.json().obj as LineaContrato)
            .catch(this.handleError);
    }

// ************
// *** POST ***
// ************
    postLineaContrato(nombreLineaContrato: string, idTipoInspeccion: string): Promise<LineaContrato> {
        return this.http.post(this.lineaContratoURL + '/',
        JSON.stringify({ nombre: nombreLineaContrato, tipoInspeccion: idTipoInspeccion}), { headers: this.headers })
            .toPromise()
            .then(response => response.json().obj as LineaContrato)
            .catch(this.handleError);
    }

// *************
// *** PATCH ***
// *************
    patchLineaContrato(idLineaDeContrato: string, nombreLinea: string, idTipoInspeccion: string): Promise<LineaContrato> {
        return this.http.patch(this.lineaContratoURL + '/' + idLineaDeContrato,
        JSON.stringify({ nombre: nombreLinea, tipoInspeccion: idTipoInspeccion}),
            { headers: this.headers })
            .toPromise()
            .then(response => response.json().obj as LineaContrato)
            .catch(this.handleError);
    }

// **************
// *** DELETE ***
// **************
    deleteLineaDeContrato(idLineaDeContrato: string): Promise<LineaContrato> {
        return this.http.delete(this.lineaContratoURL + '/' + idLineaDeContrato, { headers: this.headers })
            .toPromise()
            .then(response => response.json().obj as LineaContrato)
            .catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
        console.error('Ocurrio un error en servicio de Linea de Contrato: ', error);
        Swal.fire(
            'Error!',
            error.json().error,
            'error'
        );
        return Promise.reject(error.message || error);
    }
}
