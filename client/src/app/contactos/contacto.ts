export class Contacto {
    _id: string;
    nombre: string;
    apellido: string;
    telefono: string;
    email: string;
    rol: string;
    sector: string;
}