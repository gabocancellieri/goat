import { Component, OnInit } from '@angular/core';
import { CertificadoService } from './certificado.service';
import { Certificado } from './certificado';

@Component({
    selector: 'app-certificados',
    templateUrl: './certificados.component.html'
})

export class CertificadosComponent implements OnInit {

    certificados: Certificado[];

    constructor(
        private certificadoService: CertificadoService
    ) { }

    ngOnInit() {

    }

// ***********
// *** GET ***
// ***********
    getCertificados(): void {
        this.certificadoService
        .getCertificados()
        .then(certificados => {
            this.certificados = certificados;
        });
    }
}
