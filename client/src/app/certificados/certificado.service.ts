import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';

import 'rxjs/add/operator/toPromise';

import { Certificado } from './certificado';
import { Counter } from './counter';
import { Tanque } from '../tanques/tanque';
import { Usuario} from '../usuario/usuario';


import {UsuarioService} from '../usuario/user.service';
import {Config} from '../config';
import { UrlService } from '../window.provider.service';

@Injectable()
export class CertificadoService {

    private headers = new Headers({'Content-Type': 'application/json'});
    private certificadosURL = this.urlService.getRestApiUrl() + '/certificado';  // URL to web api

    constructor(
        private http: Http,
        private userService: UsuarioService,
        private urlService: UrlService) { }

    private jwt() {
        return this.userService.jwt();
    }

    getUsuarios(): Promise<Usuario[]> {
        return this.userService.getUsuarios();
    }

    getCertificado(idOat: string): Promise<Certificado> {
        return this.http.get(this.certificadosURL + '/' + idOat, this.jwt())
        .toPromise()
        .then(response => response.json().obj as Certificado)
        .catch(this.handleError);
    }
    getCertificados(): Promise<Certificado[]> {
        return this.http.get(this.certificadosURL, this.jwt())
        .toPromise()
        .then(response => response.json() as Certificado[])
        .catch(this.handleError);
    }

    getCertificadosEspecifico(numProy: String): Promise<Certificado[]> {
        return this.http.get(this.certificadosURL + '/proyecto/' + numProy, this.jwt())
        .toPromise()
        .then(response => {
            return response.json().obj as Certificado[];
        })
        .catch(this.handleError);
    }

    getCertificadoEspecifico(numProy: String, numOat: String): Promise<Certificado> {
        return this.http.get(this.certificadosURL + '/' + numProy + '/' + numOat, this.jwt())
        .toPromise()
        .then(response => response.json().obj as Certificado)
        .catch(this.handleError);
    }

    updateCertificado(id: string,  fechaRea: Date, descp: string, tipoInsp: string,
                tanque: string, usuarios: string[], nombre: string ): Promise<Certificado> {
        return this.http
        .put(this.certificadosURL + '/' + id, JSON.stringify(
            {fechaRealizacion: fechaRea, descripcion: descp, tipoInspeccion: tipoInsp,
            tanque: tanque, usuarios: usuarios, nombre: nombre }),
            {headers: this.headers})
        .toPromise()
        .then(res => {
            return res.json().obj;
        })
        .catch(this.handleError);
    }

    updateContratoCertificado(idOat: string, idContrato: string) {
        return this.http
        .put(this.certificadosURL + '/' + idOat + '/updateContrato', JSON.stringify(
            { contrato: idContrato}),
            {headers: this.headers})
        .toPromise()
        .then(res => {
            return res.json().obj;
        })
        .catch(this.handleError);
    }


    getCertificadoCounter(idCounter: string): Promise<Counter> {
        const counterURL = this.urlService.getRestApiUrl() + '/counter/';
        return this.http.get(counterURL + idCounter, this.jwt())
        .toPromise()
        .then(response => response.json().obj as Counter)
        .catch(this.handleError);
    }

    updateCounter(idCounter: string): Promise<Counter> {
        const counterURL = this.urlService.getRestApiUrl() + '/counter/';
        return this.http.put(counterURL + idCounter, {}, {headers: this.headers})
        .toPromise()
        .then(response => response.json().obj as Counter)
        .catch(this.handleError);
    }

    createCertificado(nombre: string, tipoSelectedId: string , fechaCrea: Date,
        fechaRea: Date, descp: string, idPlanta: string, idContrato: string, numProy: String, tanque: Tanque, idUsuarios: string[],
        numeroTramite: string, fechaInspeccion: Date, fechaVencimiento: Date): Promise<Certificado> {

        // Chequeamos si vino el contratoService
        let contrato = '';
        if (idContrato != null) {
            contrato = idContrato;
        }
        const idUsuarioLogueado = this.userService.getUserLogueado()._id;
        const tanq = (tanque !== undefined) ? tanque._id : null;
        const params = JSON.stringify(
        {nombre: nombre, proyecto: numProy,  fechaCreacion : fechaCrea, fechaRealizacion: fechaRea,
        descripcion: descp, planta: idPlanta, contrato: contrato, tipoInspeccion: tipoSelectedId,
        tanque: tanq, usuarioLogueado: idUsuarioLogueado, usuarios: idUsuarios,
        numeroTramite: numeroTramite, fechaInspeccion: fechaInspeccion, fechaVencimiento: fechaVencimiento});

        return this.http.post(this.certificadosURL + '/', params, {headers: this.headers}).toPromise()
        .then(response => response.json().obj as Certificado)
        .catch(this.handleError);

    }

    deleteCertificado(id: string, idProyecto: string): Promise<any> {
        return this.http
        .delete(this.certificadosURL + '/' + id + '/' + idProyecto,
            {headers: this.headers})
        .toPromise()
        .then(res => {
            return res.json().obj;
        })
        .catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
        console.error('Ocurrio un error en servicio de Certificados: ', error);
        alert(error.json().error);
        return Promise.reject(error.message || error);
    }
}