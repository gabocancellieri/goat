import { Planta } from '../plantas/planta';
import { Contrato } from '../contratos/contrato';
import { Tanque } from '../tanques/tanque';
import { Usuario } from '../usuario/usuario';
import { LineaContrato } from '../lineasContrato/lineaContrato';
import { Informe } from '../informes/informe';

export class Certificado {
    _id: string;
    nombre: string;
	fechaCreacion: Date;
	fechaRealizacion: Date;
	descripcion: String;
	planta: Planta;
	contrato: Contrato;
	tanque: Tanque;
	informes: [Informe];
	usuarios: [Usuario];
	usuarioLogueado: Usuario;
	lineaContrato: LineaContrato;
	porcentaje: number;
	cerrado: Boolean;
    fechaCierre: Date
}