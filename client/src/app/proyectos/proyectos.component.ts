import { Component, OnInit } from '@angular/core';
import { ProyectoService } from './proyecto.service';
import { Proyecto } from './proyecto';

@Component({
    selector: 'app-proyectos',
    templateUrl: './proyectos.component.html'
})

export class ProyectosComponent implements OnInit {

    proyectos: Proyecto[];

    constructor(
        private proyectoService: ProyectoService
    ) { }

    ngOnInit() {

    }

// ***********
// *** GET ***
// ***********
    getProyectos(): void {
        this.proyectoService
        .getProyectos()
        .then(proyectos => {
            this.proyectos = proyectos;
        });
    }
}
