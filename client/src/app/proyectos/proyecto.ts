import { Empresa } from '../empresas/empresa';

export class Proyecto {
    _id: string;
    nombre: string;
	fecha: Date;
	descripcion: string;
	empresas: [Empresa];
	
}