import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions, Response } from '@angular/http';

import 'rxjs/add/operator/toPromise';

import { Proyecto } from './proyecto';
import {UsuarioService} from '../usuario/user.service';

import {Config} from '../config';
import { UrlService } from '../window.provider.service';

@Injectable()
export class ProyectoService {

    private headers = new Headers({'Content-Type': 'application/json'});
    private proyectoURL = this.urlService.getRestApiUrl() + '/proyecto';  // URL to web api

    constructor(
        private http: Http,
        private userService: UsuarioService,
        private urlService: UrlService
        ) { }

    private jwt() {
        return this.userService.jwt();
    }

    getProyectos(): Promise<Proyecto[]> {
        return this.http.get(this.proyectoURL, this.jwt())
        .toPromise()
        .then(response => response.json().obj as Proyecto[])
        .catch(this.handleError);
    }

    getProyecto(id: string): Promise<Proyecto> {
        return this.http.get(this.proyectoURL + '/' + id, this.jwt())
        .toPromise()
        .then(response => response.json().obj as Proyecto)
        .catch(this.handleError);
    }

    postProyecto(nomb: string, f: Date, descrip: string, emp: string[]): Promise<Proyecto> {
        return this.http
        .post(this.proyectoURL, JSON.stringify({nombre: nomb, fecha: f, descripcion: descrip, empresas: emp }), {headers: this.headers})
        .toPromise()
        .then(res => {
            return res.json().obj;
        })
        .catch(this.handleError);
    }

    updateProyecto(id: string, nomb: string, f: Date, descrip: string, emp: string[]): Promise<Proyecto> {
        return this.http
        .put(this.proyectoURL + '/' + id, JSON.stringify(
            {_id: id, nombre: nomb, fecha: f, descripcion: descrip, empresas: emp}), {headers: this.headers})
        .toPromise()
        .then(res => {
            return res.json().obj;
        })
        .catch(this.handleError);
    }

    deleteProyecto(id: string): Promise<any> {
        return this.http
        .delete(this.proyectoURL + '/' + id,
            {headers: this.headers})
        .toPromise()
        .then(res => {
            return res.json().obj;
        })
        .catch(this.handleError);

    }

    private handleError(error: any): Promise<any> {
        console.error('Ocurrio un error en servicio de Proyecto: ', error);

        alert(error.json().error);


        return Promise.reject(error.message || error);
    }
}