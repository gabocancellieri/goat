import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

// PRIMENG
import { TableModule } from 'primeng/table';
import {ButtonModule} from 'primeng/button';
import {DialogModule} from 'primeng/dialog';

// COMPONENTES
import { InicioComponent } from './inicio/inicio.component';
import { UsuariosComponent } from './usuario/usuarios.component';
import { LoginComponent } from './login/login.component';
import { ContratosComponent } from './contratos/contratos.component';
import { ProyectosComponent } from './proyectos/proyectos.component';
import { LineasContratoComponent } from './lineasContrato/lineasContrato.component';
import { CertificadosComponent } from './certificados/certificados.component';
import { EmpresasComponent } from './empresas/empresas.component';

// SERVICIOS
import { AuthenticationService } from './auth/auth.service';
import { UrlService } from './window.provider.service';
import { WINDOW_PROVIDERS } from './window.provider';
import { AuthGuard } from './auth/auth.guard';
import { AlertService } from './alert/alert.service';
import { UsuarioService } from './usuario/user.service';
import { PermissionService } from './usuario/permission.service';
import { ContratoService } from './contratos/contrato.service';
import { ProyectoService } from './proyectos/proyecto.service';
import { LineaContratoService } from './lineasContrato/lineaContrato.service';
import { TipoInspeccionService } from './lineasContrato/tipoInspeccion/tipoInspeccion.service';
import { CertificadoService } from './certificados/certificado.service';
import { EmpresaService } from './empresas/empresa.service';

@NgModule({
  declarations: [
    AppComponent,
    InicioComponent,
    UsuariosComponent,
    LoginComponent,
    ContratosComponent,
    ProyectosComponent,
    LineasContratoComponent,
    CertificadosComponent,
    EmpresasComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    FormsModule,
    HttpModule,
    TableModule,
    ButtonModule,
    DialogModule
  ],
  providers: [
    AuthenticationService,
    UrlService,
    WINDOW_PROVIDERS,
    AuthGuard,
    AlertService,
    UsuarioService,
    PermissionService,
    ContratoService,
    ProyectoService,
    LineaContratoService,
    TipoInspeccionService,
    CertificadoService,
    EmpresaService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
