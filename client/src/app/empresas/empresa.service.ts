import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';

import 'rxjs/add/operator/toPromise';
import { Empresa } from './empresa';
import {UsuarioService} from '../usuario/user.service';

import Swal from 'sweetalert2';
import {Config} from '../config';
import { UrlService } from '../window.provider.service';

@Injectable()
export class EmpresaService {

    private headers = new Headers({'Content-Type': 'application/json'});
    private empresasURL = this.urlService.getRestApiUrl() + '/empresa';  // URL to web api

    constructor(
        private http: Http,
        private userService: UsuarioService,
        private urlService: UrlService
        // private alertErrorService: AlertErrorService)
    ) { }

    private jwt() {
        return this.userService.jwt();
    }

    getEmpresaEspecifica(id: string): Promise<Empresa> {

        return this.http.get(this.empresasURL + '/' + id, this.jwt())
        .toPromise()
        .then(response => response.json().obj as Empresa)
        .catch(this.handleError);
    }

    downloadFile(id: string): Promise<any> {
        return this.http.get(this.empresasURL + '/download/' + id, this.jwt())
        .toPromise()
        .then(res => {
            return res;
        })
        .catch(this.handleError);
    }

    getEmpresas(): Promise<Empresa[]> {

        return this.http.get(this.empresasURL, this.jwt())
        .toPromise()
        .then(response => response.json().obj as Empresa[])
        .catch(this.handleError);
    }

    updateEmpresa(id: string , nomb: string, nuevoCuit: string, direccion: string, num_calle: number,
                    loc: string, prov: string, cod_postal: number, telefono: string): Promise<Empresa> {
        return this.http
        .patch(this.empresasURL, JSON.stringify({_id: id, nombre: nomb, cuit: nuevoCuit, calle: direccion, numero_calle: num_calle,
                                                localidad: loc, provincia: prov, codigo_postal: cod_postal,
                                                tel: telefono}), {headers: this.headers})
        .toPromise()
        .then(res => {
            return res.json().obj;
        })
        .catch(this.handleError);
    }

    updateConstancia(idEmpresa: string, constanciaAFIP: File): Promise<Empresa> {
        const size = constanciaAFIP.size;
        const name = constanciaAFIP.name;
        return this.http
        .patch(this.empresasURL + '/constancia/' + idEmpresa, JSON.stringify({fileName: name, fileSize: size}), {headers: this.headers})
        .toPromise()
        .then(res => {
            const fd = new FormData();
            fd.append('constanciaAFIP', constanciaAFIP, res.json().obj.nombre + ".pdf");
            this.http
            .post(this.empresasURL + '/upload', fd)
            .toPromise()
            .then(response => {})
            .catch(this.handleError);

            return res.json().obj as Empresa;
        })
        .catch(this.handleError);
    }

    createEmpresa(nomb: string, nuevoCuit: string, direccion: string, num_calle: number,
                loc: string, prov: string, cod_postal: number, telefono: string, constanciaAFIP: File): Promise<Empresa> {
        let size = null;
        let name = null;
        if (constanciaAFIP) {
            size = constanciaAFIP.size;
            name = constanciaAFIP.name;
        }

        return this.http
            .post(this.empresasURL, JSON.stringify({nombre: nomb, cuit: nuevoCuit, calle: direccion, numero_calle: num_calle,
                                                    localidad: loc, provincia: prov, codigo_postal: cod_postal, tel: telefono,
                                                    fileName: name, fileSize: size}), {headers: this.headers})
            .toPromise()
            .then(res => {
                // La Empresa se cargo correctamente
                // Pasamos a subir el archivo, si lo hay
                if (constanciaAFIP) {
                    const fd = new FormData();
                    fd.append('constanciaAFIP', constanciaAFIP, nomb + ".pdf");
                    this.http
                    .post(this.empresasURL + '/upload', fd)
                    .toPromise()
                    .then(res => {})
                    .catch(this.handleError);
                }
                return res.json().obj;
            })
            .catch(this.handleError);
    }

    deleteEmpresa(idEmpresa: string) {
        const opciones = this.jwt();
        opciones.headers.append('Content-Type', 'application/json');
        return this.http
        .delete(this.empresasURL + '/' + idEmpresa,
        opciones)
        .toPromise()
        .then(res => {
            return res.json().obj;
        })
        .catch(this.handleError);
    }

    deleteConstancia(idEmpresa: string) {
        const opciones = this.jwt();
        opciones.headers.append('Content-Type', 'application/json');
        return this.http
        .delete(this.empresasURL + '/constancia/' + idEmpresa,
        opciones)
        .toPromise()
        .then(res => {
            return res.json().obj;
        })
        .catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
        // this.alertErrorService.handleError('Empresa',error);
        console.error('Ocurrio un error en servicio de Empresas: ', error);
        Swal.fire(
            'Error!',
            error.json().error,
            'error'
        );
        return Promise.reject(error.message || error);
    }
}
