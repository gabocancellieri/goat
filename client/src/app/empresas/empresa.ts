import { Planta } from '../plantas/planta';
import { Contacto } from '../contactos/contacto';

export class Empresa {
    _id: string;
    nombre: string;
    cuit: string;
    calle: string;
    numeroCalle: number;
    localidad: string;
    provincia: string;
    codigoPostal: number;
    plantas: [Planta];
    contactos: [Contacto];
    constanciaAFIP: string
}