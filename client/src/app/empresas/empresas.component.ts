import { Component, OnInit } from '@angular/core';
import { EmpresaService } from './empresa.service';
import { Empresa } from './empresa';

@Component({
    selector: 'app-empresas',
    templateUrl: './empresas.component.html'
})

export class EmpresasComponent implements OnInit {

    empresas: Empresa[];

    constructor(
        private empresaService: EmpresaService
    ) { }

    ngOnInit() {

    }

// ***********
// *** GET ***
// ***********
    getEmpresas(): void {
        this.empresaService
        .getEmpresas()
        .then(empresas => {
            this.empresas = empresas;
        });
    }
}
