import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './auth/auth.guard';

// COMPONENTES
import { InicioComponent } from './inicio/inicio.component';
import { UsuariosComponent } from './usuario/usuarios.component';
import { LoginComponent } from './login/login.component';
import { CertificadosComponent } from './certificados/certificados.component';
import { ContratosComponent } from './contratos/contratos.component';
import { ProyectosComponent } from './proyectos/proyectos.component';
import { LineasContratoComponent } from './lineasContrato/lineasContrato.component';
import { EmpresasComponent } from './empresas/empresas.component';

const routes: Routes = [
    // REDIRECCIONAMIENTO A INICIO
    { path: '', redirectTo: '/inicio', pathMatch: 'full' },

    // RUTAS A COMPONENTES
    { path: 'inicio', component: InicioComponent, canActivate: [AuthGuard] },
    { path: 'certificados', component: CertificadosComponent, canActivate: [AuthGuard] },
    { path: 'contratos', component: ContratosComponent, canActivate: [AuthGuard] },
    { path: 'lineas-contrato', component: LineasContratoComponent, canActivate: [AuthGuard] },
    { path: 'proyectos', component: ProyectosComponent, canActivate: [AuthGuard] },
    { path: 'empresas', component: EmpresasComponent, canActivate: [AuthGuard] },
    { path: 'usuarios',  component: UsuariosComponent, canActivate: [AuthGuard]},
    { path: 'login', component: LoginComponent }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
