import { Certificado } from 'src/app/certificados/certificado';

export class Hes {
    _id: string;
    numero: string;
	fecha: Date;
	descripcion: string;
	porcentajeCertificados: [{
		certificado: Certificado;
		porcentaje: number
	}];
	monto: number;
	tipo: string
}