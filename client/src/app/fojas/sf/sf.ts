export class SF {
    _id: string;
    numero: string;
	fecha: Date;
	monto: number;
	descripcion: string;
	numerosHes: [string]
}