import { Empresa } from '../empresas/empresa';
import { Contrato } from '../contratos/contrato';
import { Certificado } from '../certificados/certificado';
import { Hes } from './hes/hes';
import { SF } from './sf/sf';

export class Foja {
    _id: string;
    numero: string;
	fecha: Date;
	empresa: Empresa;
	contrato: Contrato;
	porcentaje: number;
	descripcion: string;
	monto: number;
	movilidad: boolean;
	certificados: [Certificado];
	hojas: [Hes];
	solicitudes: [SF];
}