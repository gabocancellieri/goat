'use strict';

/**
* Dependencias del modulo
*/
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var Permisssion = require('./permission');


/**
* Usuario Schema
*/
var usuario_schema = new Schema({
	username: String,
    password: String,
    firstName: String,
    lastName: String,
    permissions: [{type: Schema.Types.ObjectId, ref: 'Permission'}]
});


// El esquema solo no sirve. Luego, creamos el modelo
var Usuario = mongoose.model('Usuario',usuario_schema);

module.exports = Usuario;
