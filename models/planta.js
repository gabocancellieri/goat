'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var plantaShema = new Schema({
    nombre: String,
    ubicacion: String,
    responsable: String,
    contacto: String,
    tanques: [{
        type: Schema.Types.ObjectId,
        ref: 'Tanque'
    }],
    encargado: {
        type: Schema.Types.ObjectId,
        ref: 'Contacto'
    }
});

var Planta = mongoose.model('Planta', plantaShema);

module.exports = Planta;