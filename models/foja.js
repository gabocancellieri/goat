'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var SF = new Schema({
	numero: String,
	fecha: Date,
	monto: Number,
	descripcion: String,
	numerosHes: [String]
});

var Hes = new Schema({
	numero: {type: String},
	fecha: Date,
	descripcion: String,
	porcentajeCertificados: [{
		certificado: {type: Schema.Types.ObjectId, ref: 'Certificado'},
		porcentaje: Number
	}],
	monto: {type: Number, default: 0},
	tipo: String
});


var fojaSchema = new Schema({
	numero: {type: String, unique: true},
	fecha: Date,
	empresa: {type: Schema.Types.ObjectId, ref: 'Empresa'},
	contrato: {type: Schema.Types.ObjectId, ref: 'Contrato'},
	porcentaje: {type: Number},
	descripcion: {type: String},
	monto: {type: Number},
	movilidad: {type: Boolean},
	certificados: [{type: Schema.Types.ObjectId, ref: 'Certificado'}],
	hojas: [Hes],
	solicitudes: [SF]
});

var Foja = mongoose.model('Foja', fojaSchema);

module.exports = Foja;