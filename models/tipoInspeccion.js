'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var tipoInspeccionSchema = new Schema({
    nombre: {
        type: String,
        unique: true
    },
    descripcion: String,
    tanque: Boolean
});

var TipoInspeccion = mongoose.model('TipoInspeccion', tipoInspeccionSchema);

module.exports = TipoInspeccion;