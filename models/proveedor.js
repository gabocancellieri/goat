'use strict';

/**
 * Dependencias del modulo
 */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;


/**
 * Proveedor Schema
 */
var proveedor_schema = new Schema({
	nombre: String,
	direccion: String,
	telefono: String,
	email: String,
	contacto: String,
	telefonoContacto: String,
	cuit: String
});


// El esquema solo no sirve. Luego, creamos el modelo
var Proveedor = mongoose.model('Proveedor',proveedor_schema);


//module.exports.Proveedor = Proveedor;

module.exports = Proveedor;
