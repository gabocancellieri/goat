'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Ajuste = new Schema({
	precio: Number,
	fecha: Date
});

var lineaSchema = new Schema({
    lineaContrato: {
		type: Schema.Types.ObjectId,
		ref: 'LineaContrato'
	},
	ajustes: [Ajuste]
});

var Linea = mongoose.model('Linea', lineaSchema);

module.exports = Linea;