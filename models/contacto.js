'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var contactoSchema = new Schema({
    nombre: String,
    apellido: String,
    telefono: String,
    email: String,
    rol: String,
    sector: String
});

var Contacto = mongoose.model('Contacto', contactoSchema);

module.exports = Contacto;