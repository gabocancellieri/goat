'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var tipoContratoSchema = new Schema({
    nombre: {
        type: String,
        unique: true
    }
});

var TipoContrato = mongoose.model('TipoContrato', tipoContratoSchema);

module.exports = TipoContrato;