'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var empresaSchema = new Schema({
    nombre: {type: String, unique: true},
    cuit: {type: String, unique: true},
    calle: String,
    numeroCalle: Number,
    localidad: String,
    provincia: String,
    codigoPostal: Number,
    plantas: [{type: Schema.Types.ObjectId, ref: 'Planta'}],
    contactos: [{ type: Schema.Types.ObjectId, ref: 'Contacto'}],
    constanciaAFIP: String
});

var Empresa = mongoose.model('Empresa', empresaSchema);

module.exports = Empresa;