'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Adenda = new Schema({
	fechaAdenda: Date,
	fechaFin: Date,
	montoTotal: Number,
	lineas: [{
		type: Schema.Types.ObjectId,
		ref: 'Linea'
	}]
});

var contratoSchema = new Schema({
	nombre: {
		type: String,
		unique: true
	},
	numero: {
		type: String,
		unique: true
	},
	fecha: Date,
	fechaInicio: Date,
	formAjuste: String,
	tipo: {
		type: Schema.Types.ObjectId,
		ref: 'TipoContrato'
	},
	proyecto: {
		type: Schema.Types.ObjectId,
		ref: 'Proyecto'
	},
	empresa: {
		type: Schema.Types.ObjectId,
		ref: 'Empresa'
	},
	certificados: [{
		type: Schema.Types.ObjectId,
		ref: 'Certificado'
	}],
	fojas: [{
		type: Schema.Types.ObjectId,
		ref: 'Foja'
	}],
	adendas: [Adenda]
});

var Contrato = mongoose.model('Contrato', contratoSchema);

module.exports = Contrato;