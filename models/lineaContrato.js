'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var lineaContratoSchema = new Schema({
    nombre: {
        type: String,
        unique: true
    },
    tipoInspeccion: {
      type: Schema.Types.ObjectId,
      ref: 'TipoInspeccion'
    }
});

var LineaContrato = mongoose.model('LineaContrato', lineaContratoSchema);

module.exports = LineaContrato;