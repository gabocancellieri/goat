'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var proyectoSchema = new Schema({
	nombre: { type: String, index: { unique: true } },
	fecha: Date,
	descripcion: String,
	empresas: [{type: Schema.Types.ObjectId, ref: 'Empresa'}],
});

var Proyecto = mongoose.model('Proyecto', proyectoSchema);

module.exports = Proyecto;
