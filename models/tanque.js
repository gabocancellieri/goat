'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var tanqueSchema = new Schema({
	cit: {type : String, unique:true},
	tag: {type : String, unique:true}
});

var Tanque = mongoose.model('Tanque',tanqueSchema);

module.exports = Tanque;