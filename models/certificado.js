'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Revision = new Schema({
	numero: String,
	fecha: Date,
	observaciones: String,
	usuario: {
		type: Schema.Types.ObjectId,
		ref: 'Usuario'
	}
});

var Version = new Schema({
	nombre: String,
	fecha: Date,
	descripcion: String,
	revisiones: [Revision]
});

var Sgda = new Schema({
	numeroTramite: String,
	fechaInspeccion: Date,
	fechaVencimiento: Date
});

var certificadoschema = new Schema({
	nombre: {type: String, unique: true},
	fechaCreacion: Date,
	fechaRealizacion: Date,
	descripcion: String,
	planta: {type: Schema.Types.ObjectId, ref: 'Planta'},
	contrato: {type: Schema.Types.ObjectId, ref: 'Contrato'},
	tanque: {type: Schema.Types.ObjectId, ref: 'Tanque'},
	versiones: [Version],
	sgda: Sgda,
	usuarios: [{type: Schema.Types.ObjectId, ref: 'Usuario'}],
	usuarioLogueado: {type: Schema.Types.ObjectId, ref: 'Usuario'},
	lineaContrato: {type: Schema.Types.ObjectId, ref: 'LineaContrato'},
	porcentaje: {type: Number, default: 0, max: [100, 'El porcentaje no puede ser mayor que 100%']},
	cerrado: Boolean,
	fechaCierre: Date
});

var Certificado = mongoose.model('Certificado', certificadoschema);

module.exports = Certificado;