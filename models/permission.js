'use strict';


/**
* Dependencias del modulo
*/
var mongoose = require('mongoose');
var Schema = mongoose.Schema;



/**
* Contrato Schema
*/
var permission_schema = new Schema({
    name:{ type: String, unique:true },
    description: String,
});


// El esquema solo no sirve. Luego, creamos el modelo
var Permission = mongoose.model('Permission',permission_schema);

module.exports = Permission;
