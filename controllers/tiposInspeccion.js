var TiposInspeccion = require('../models/tipoInspeccion');

function getTiposInspeccion(req,res){
    var query = TiposInspeccion.find({});
    query.exec(
    function(err, tiposInspeccion){
        if (err) {
            return res.status(404).json({
                title: 'Error',
                error: err
            });
        }
        res.status(200).json({
            message: 'Success',
            obj: tiposInspeccion
        });
    });
}

module.exports = {
    getTiposInspeccion
};