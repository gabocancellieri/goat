var Contrato = require('../models/contrato');
var Linea = require('../models/linea');

function getContratos(req,res){
    var query = Contrato.find({});
    query.exec(
    function(err,contratos){
        if (err) {
            return res.status(404).json({
                title: 'Error',
                error: err
            });
        }
        res.status(200).json({
            message: 'Success',
            obj: contratos
        });
    });
}

function getContrato(req,res){

    var query =    Usuario.findById(req.params.userId)

     query.populate('permissions').exec(
    function(err,usr){
        if (err) {
            return res.status(404).json({
                title: 'An error occurred',
                error: err
            });
        }
        if(!usr){
            return res.status(404).json({
                title: 'Error',
                error: 'Usuario no encontrado'
            });
        }
        usr.password="********";
        res.status(200).json({
            message: 'Success',
            obj: usr
        });
    });
}

function patchContrato(req, res){
    if(!req.body.firstName){
        return res.status(400).json({
            title: 'Error',
            error: 'Nombre vacío.'
        });
    }
    
    var query = Usuario.findOne({'_id':req.body._id});

        query.exec(function (err, user) {
            if (err) {
                return res.status(404).json({
                    title: 'An error occurred',
                    error: err
                });
            }
            if(!user){
                return res.status(404).json({
                    title: 'Error',
                    error: 'Usuario no encontrado'
                });
            }

            if(req.body.password && req.body.password.length>0){
                user.password = req.body.password;
            }
            user.firstName = req.body.firstName;
            user.lastName = req.body.lastName;
            user.username = req.body.username;
            user.permissions= req.body.permissions;

            user.save().then(function(loc){
                res.status(200).json({
                    message: 'Success',
                    obj: loc
                });
            },function(err){
                return res.status(404).json({
                    title: 'Error',
                    error: err
                });
            });
        });

    }


function postContrato(req,res){
    var usuario = new Usuario({
        username: req.body.username,
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        password: req.body.password
    });

    usuario.save().then(function(usuario){
        res.status(201).json({
            message: 'Usuario creado',
            obj: usuario
        });
    },function(err){
        if(err.code==11000){
            var msj=""
            //Catching index name inside errmsg reported by mongo to determine the correct error and showing propper message
            if(err.errmsg.toString().includes("username"))
                msj= "Nombre de Usuario (username)";
            else if(err.errmsg.toString().includes("firstName"))
                msj="Nombre de usuario";

            return res.status(404).json({
                title: 'Error',
                error: msj+' existente.'
            });
        }
        return res.status(404).json({
            title: 'Error',
            error: err
        });
    });
}

function deleteContrato(req,res){
    if (!req.params.idUsuario) {
        return res.status(400).json({
            title: 'Error',
            error: 'Falta enviar el id del usuario.'
        });
    }

    Usuario.findOne({'_id': req.params.idUsuario})
    .exec(function (error, usuario){
        if (error) {
            return res.status(400).json({
                title: 'Error',
                error: 'Ocurrio un error.'
            });
        }

        if (!usuario) {
            return res.status(404).json({
                title: 'Error',
                error: 'No existe tal usuario.'
            });
        }

        usuario.remove().then(function (usu) {
            return res.status(200).json({
                message: 'Usuario eliminado correctamente',
                obj: usu
            });
        }, function (err) {
            return res.status(400).json({
                title: 'Error',
                error: err.message
            });
        })
    })
}

module.exports = {
    getContratos,
    getContrato,
    postContrato,
    patchContrato,
    deleteContrato
};