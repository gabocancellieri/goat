var Empresa = require('../models/empresa');
var Proyecto = require('../models/proyecto');

function getEmpresas(req, res){
    var query = Empresa.find({});

    query.exec(
    function(error, empresas){
        if (error) {
            return res.status(404).json({
                title: 'Error',
                error: err
            });
        }
        res.status(200).json({
            message: 'Success',
            obj: empresas
        });
    });
}

function getEmpresa(req,res){
    var query = Empresa.findById(req.params.idEmpresa);

    query.exec(
    function(error, empresa){
        if (error) {
            return res.status(404).json({
                title: 'Error',
                error: err
            });
        }

        if (!empresa) {
            return res.status(404).json({
                title: 'Error',
                error: 'Empresa no encontrada'
            });
        }

        res.status(200).json({
            message: 'Success',
            obj: empresa
        });
    });
}

function patchEmpresa(req, res){
    if (!req.body._id) {
        return res.status(400).json({
            title: 'Error',
            error: 'ID vacío.'
        });
    }

    if (!req.body.nombre) {
        return res.status(400).json({
            title: 'Error',
            error: 'Nombre vacío.'
        });
    }
    if (!req.body.cuit) {
        return res.status(400).json({
            title: 'Error',
            error: 'CUIT vacío.'
        });
    }
    
    var query = Empresa.findOne({
        '_id': req.body._id
    });

    query.exec(function (err, empresa) {
        if (err) {
            return res.status(404).json({
                title: 'An error occurred',
                error: err
            });
        }
        if (!empresa) {
            return res.status(404).json({
                title: 'Error',
                error: 'Empresa no encontrada'
            });
        }


        empresa.nombre = req.body.nombre;
        empresa.cuit = req.body.cuit;
        empresa.calle = req.body.calle;
        empresa.numeroCalle = req.body.numeroCalle;
        empresa.localidad = req.body.localidad;
        empresa.provincia = req.body.provincia;
        empresa.codigoPostal = req.body.codigoPostal;

        empresa.save().then(function (emp) {
            res.status(200).json({
                message: 'Success',
                obj: emp
            });
        }, function (err) {
            return res.status(404).json({
                title: 'Error',
                error: err
            });
        });
    });
}


function postEmpresa(req,res){
    if (!req.body.nombre) {
        return res.status(400).json({
            title: 'Error',
            error: 'Nombre vacío.'
        });
    }
    if (!req.body.cuit) {
        return res.status(400).json({
            title: 'Error',
            error: 'CUIT vacío.'
        });
    }

    if (req.body.fileName) {
        var allowedExtension = /(\.pdf)$/i;
        if (!allowedExtension.exec(req.body.fileName)) {
            return res.status(400).json({
                title: 'Error',
                error: 'El archivo no es un PDF.'
            });
        }
        var consAFIP = "CONSTANCIA AFIP - " + req.body.nombre + ".pdf";
    }

    if (req.body.fileSize) {
        var allowedSize = 20 * 1024 * 1024;
        if (req.body.fileSize > allowedSize) {
            return res.status(400).json({
                title: 'Error',
                error: 'Archivo muy grande! Limite: 20Mb.'
            });
        }
    }

    var empresa = new Empresa({
        nombre: req.body.nombre,
        cuit: req.body.cuit,
        calle: req.body.calle,
        numeroCalle: req.body.numeroCalle,
        localidad: req.body.localidad,
        provincia: req.body.provincia,
        codigoPostal: req.body.codigoPostal,
        constanciaAFIP: consAFIP
    });

    empresa.save().then(function (empresa) {
        res.status(201).json({
            message: 'Empresa creada',
            obj: empresa
        });

    }, function (err) {
        if (err.code == 11000) {
            var msj = ""
            if (err.errmsg.toString().includes("nombre"))
                msj = "Nombre de Empresa";
            else if (err.errmsg.toString().includes("cuit"))
                msj = "Número de CUIT";

            return res.status(404).json({
                title: 'Error',
                error: msj + ' existente.'
            });
        }
        return res.status(404).json({
            title: 'Error',
            error: err
        });
    });
}

function deleteEmpresa(req,res){
    Proyecto.find({'empresas':req.params.idEmpresa}, function (error, proyectos) {
        if (error) {
            return res.status(404).json({
                title: 'Error',
                error: err
            });
        }

        if (proyectos.length != 0) {
            return res.status(400).json({
                title: 'Error',
                error: 'Esta Empresa está asociada a un Proyecto'
            });
        }

        Empresa.findOne({'_id': req.params.idEmpresa})
        .exec(function (error, empresa){
            if (error) {
                return res.status(400).json({
                    title: 'Error',
                    error: 'Ocurrio un error.'
                });
            }
    
            if (!empresa) {
                return res.status(404).json({
                    title: 'Error',
                    error: 'No existe tal Empresa.'
                });
            }
    
            empresa.remove().then(function (emp) {
                return res.status(200).json({
                    message: 'Empresa eliminada correctamente',
                    obj: emp
                });
            }, function (err) {
                return res.status(400).json({
                    title: 'Error',
                    error: err.message
                });
            })
        })
    })
}

module.exports = {
    getEmpresas,
    getEmpresa,
    postEmpresa,
    patchEmpresa,
    deleteEmpresa
};