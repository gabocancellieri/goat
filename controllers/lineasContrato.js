var LineaContrato = require('../models/lineaContrato');
var TipoInspeccion = require('../models/tipoInspeccion');
var Linea = require('../models/linea');

function getLineasContrato(req,res){
    var query = LineaContrato.find({});
    query
    .populate('tipoInspeccion')
    .exec(
    function(err, lineasContrato){
        if (err) {
            return res.status(404).json({
                title: 'Error',
                error: err
            });
        }
        res.status(200).json({
            message: 'Success',
            obj: lineasContrato
        });
    });
}









function getLineaContrato(req,res){

    var query =    Usuario.findById(req.params.userId)

     query.populate('permissions').exec(
    function(err,usr){
        if (err) {
            return res.status(404).json({
                title: 'An error occurred',
                error: err
            });
        }
        if(!usr){
            return res.status(404).json({
                title: 'Error',
                error: 'Usuario no encontrado'
            });
        }
        usr.password="********";
        res.status(200).json({
            message: 'Success',
            obj: usr
        });
    });
}

function postLineaContrato(req,res){
    if (!req.body.nombre) {
        return res.status(400).json({
            title: 'Error',
            error: 'Nombre vacío.'
        });
    }

    var lineaContrato = new LineaContrato({
        nombre: req.body.nombre,
        tipoInspeccion: req.body.tipoInspeccion
    });

    lineaContrato.save().then(function(lineaContrato){

        TipoInspeccion.populate(lineaContrato, [{
            path: 'tipoInspeccion',
            model: 'TipoInspeccion'
        }],
            function(err, lineaContratoExpandida) {
                if (err) {
                    return res.status(400).json({
                        title: 'Error',
                        error: err
                    });
                }

                res.status(201).json({
                    message: 'Linea de Contrato creada',
                    obj: lineaContratoExpandida
                });
            }, function (err) {

                if (err) {
                    return res.status(404).json({
                        title: 'Error',
                        error: err
                    });
                }
            });
        
    },function(err){
        if(err.code==11000){
            var msj=""
            //Catching index name inside errmsg reported by mongo to determine the correct error and showing propper message
            if(err.errmsg.toString().includes("nombre"))
                msj= "Nombre de Linea de Contrato (nombre)";

            return res.status(404).json({
                title: 'Error',
                error: msj+' existente.'
            });
        }
        return res.status(404).json({
            title: 'Error',
            error: err
        });
    });
}

function patchLineaContrato(req, res){    
    var query = LineaContrato.findOne({'_id':req.params.idLineaContrato});
    query.exec(function (err, lineaContrato) {
        if (err) {
            return res.status(404).json({
                title: 'An error occurred',
                error: err
            });
        }
        if(!lineaContrato){
            return res.status(404).json({
                title: 'Error',
                error: 'Usuario no encontrado'
            });
        }

        lineaContrato.nombre = req.body.nombre;
        lineaContrato.tipoInspeccion = req.body.tipoInspeccion;

        lineaContrato.save().then(function(lineaContrato){
            res.status(200).json({
                message: 'Success',
                obj: lineaContrato
            });
        },function(err){
            return res.status(404).json({
                title: 'Error',
                error: err
            });
        });
    });
}

function deleteLineaContrato(req,res){
    if (!req.params.idLineaContrato) {
        return res.status(400).json({
            title: 'Error',
            error: 'Falta enviar el id del usuario.'
        });
    }

    Linea.find({
        'lineaContrato': req.params.idLineaContrato
    })
    .exec(function (error, linea) {
        if (error) {
            return res.status(400).json({
                title: 'Error',
                error: 'Ocurrio un error.'
            });
        }
        if (linea.length != 0) {
            return res.status(400).json({
                title: 'Error',
                error: 'Esta Linea de Contrato está asociada a un Contrato'
            });
        }
        
        LineaContrato.findOne({'_id': req.params.idLineaContrato})
        .exec(function (error, lineaContrato){
            if (error) {
                return res.status(400).json({
                    title: 'Error',
                    error: 'Ocurrio un error.'
                });
            }
    
            if (!lineaContrato) {
                return res.status(404).json({
                    title: 'Error',
                    error: 'No existe tal Linea de Contrato.'
                });
            }
    
            lineaContrato.remove().then(function (linCon) {
                return res.status(200).json({
                    message: 'Linea de Contrato eliminada correctamente',
                    obj: linCon
                });
            }, function (err) {
                return res.status(400).json({
                    title: 'Error',
                    error: err.message
                });
            })
        })
    })
}

module.exports = {
    getLineasContrato,
    getLineaContrato,
    postLineaContrato,
    patchLineaContrato,
    deleteLineaContrato
};