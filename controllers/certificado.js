var Certificado = require('../models/certificado');
var Proyecto = require('../models/proyecto');

function getCertificados(req, res){
    var query = Certificado.find({});

    query.exec(
    function(error, certificados){
        if (error) {
            return res.status(404).json({
                title: 'Error',
                error: err
            });
        }
        res.status(200).json({
            message: 'Success',
            obj: certificados
        });
    });
}

function getCertificadosConProyectos(req, res) {
    //obtenemos todos los proyectos
    var query = Proyecto.find({});

    query.populate('certificados')
        .exec(function (err, proyectosConCertificados) {
            if (err) {
                return res.status(400).json({
                    title: 'Error',
                    error: 'Error al buscar los certificados de los proyectos'
                });
            }
            var certificados = [];
            var certificadosAux = [];
            var certAux;
            var i;
            proyectosConCertificados.forEach(function (proyecto, index) {

                certificadosAux = proyecto.certificados;
                for (i = 0; i < certificadosAux.length; i++) {
                    //Hacemos una copia para poder romper la estrutctura de Mongoose
                    certAux = JSON.parse(JSON.stringify(certificadosAux[i]));

                    certAux.idProy = proyecto._id;
                    certAux.nombreProyecto = proyecto.nombre;

                    certificados.push(certAux);
                }
            });

            res.status(200).json({
                message: 'Success',
                obj: certificados
            });
        });
};

function getCertificadosProyecto(req,res){
    
}

function getCertificado(req,res){
    
}

function patchCertificado(req, res){
    
}


function postCertificado(req,res){
   
}

function deleteCertificado(req,res){
   
}

module.exports = {
    getCertificados,
    getCertificadosProyecto,
    getCertificadosConProyectos,
    getCertificado,
    postCertificado,
    patchCertificado,
    deleteCertificado
};