var Contrato = require('../models/contrato');
var Linea = require('../models/linea');

function getLineas(req, res){
    
}

function getLinea(req,res){
    
}

function patchLinea(req, res){
    
}


function postLinea(req,res){
   
}

function deleteLinea(req,res){
   
}

module.exports = {
    getLineas,
    getLinea,
    postLinea,
    patchLinea,
    deleteLinea
};