var Proyecto = require('../models/proyecto');

function getProyectos(req, res) {
    var query = Proyecto.find({});

    query.populate('empresas')
    .exec(
        function (error, proyectos) {
            if (error) {
                return res.status(404).json({
                    title: 'Error',
                    error: err
                });
            }
            res.status(200).json({
                message: 'Success',
                obj: proyectos
            });
        });
}

function getProyecto(req, res) {

}

function patchProyecto(req, res) {

}


function postProyecto(req, res) {

}

function deleteProyecto(req, res) {

}

module.exports = {
    getProyectos,
    getProyecto,
    postProyecto,
    patchProyecto,
    deleteProyecto
};