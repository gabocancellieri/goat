var Planta = require('../models/planta');
var Empresa = require('../models/empresa');

function getPlantas(req, res){
    var query = Empresa.findById(req.params.idEmpresa);

    query
    .populate('plantas')
    .exec(
    function(error, empresa){
        if (error) {
            return res.status(400).json({
                title: 'Error',
                error: error
            });
        }
        if (!empresa) {
            return res.status(404).json({
                title: 'Error',
                error: error
            });
        }
        res.status(200).json({
            message: 'Success',
            obj: empresa.plantas
        });
    });
}

function getPlanta(req,res){
    
}

function patchPlanta(req, res){
    
}


function postPlanta(req,res){
   
}

function deletePlanta(req,res){
   
}

module.exports = {
    getPlantas,
    getPlanta,
    postPlanta,
    patchPlanta,
    deletePlanta
};